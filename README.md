# 简单说明

## 这是一个gulp批处理集合 默认是合并

### 文件夹说明
#### src 放下载的zip解压
#### gulpfile 里面需要改配置
#### dist 文件夹是用来放批处理结束的文件的

### 配置说明
#### gulpfile 里面有几个配置
##### cwd 下载源码目录
##### error_java_* 异常枚举的模版
##### main_name 改包名之后的主包名
##### module_name 主包的名字
##### base_path cwd文件夹下的文件路径
##### error_start 异常处理的状态码开始
##### java_test_sql_path 测试里面的sql地址
##### java_api_err_path 异常枚举的地址

#### 加入env配置

#### 加入校验java代码import 和去掉多余import（可能与注释有冲突。就这样吧。）